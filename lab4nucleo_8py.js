var lab4nucleo_8py =
[
    [ "adc", "lab4nucleo_8py.html#a1a02c865c62e3b7c0ed2a635dc7dc358", null ],
    [ "data", "lab4nucleo_8py.html#ade0762c8d6cf3c2b00cc27bfdb253dc7", null ],
    [ "filename", "lab4nucleo_8py.html#a1ed9627ecf72043f95c973b529dd1be2", null ],
    [ "header", "lab4nucleo_8py.html#a57911a93a96f5cc56f4e1cf21af6be5e", null ],
    [ "hrs", "lab4nucleo_8py.html#a3e8158ce1f2cc6c07f5b8421b45c44fc", null ],
    [ "MCP9808", "lab4nucleo_8py.html#aed17bdaab6d0dc7464cf42c678a18e8e", null ],
    [ "t0", "lab4nucleo_8py.html#a5cdce239502e6b30e7b745120cc481bd", null ],
    [ "t_current", "lab4nucleo_8py.html#a40d1b36f75602ecd99bfe8efc14fc79c", null ],
    [ "t_iter", "lab4nucleo_8py.html#a125a446c364baff82ef0f69ec241f4a1", null ],
    [ "temp", "lab4nucleo_8py.html#a819aa1774669d0095c405a144631c2a2", null ],
    [ "temp_A", "lab4nucleo_8py.html#a05312cefc6931dfe4433aad7ca8ae9ae", null ],
    [ "ticks_0", "lab4nucleo_8py.html#a77ed16688bd534787c775322208b65f1", null ],
    [ "time_ms_run", "lab4nucleo_8py.html#abdae185c5926423ba7c1a774f07e28dc", null ],
    [ "time_s", "lab4nucleo_8py.html#a74033df43aa368e922d7f7ac33da4bfb", null ],
    [ "total_time", "lab4nucleo_8py.html#a1ddb7d8841feb05a107ca443f38e1434", null ]
];