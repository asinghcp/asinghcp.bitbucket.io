var files_dup =
[
    [ "bno055.py", "bno055_8py.html", "bno055_8py" ],
    [ "bno055_base.py", "bno055__base_8py.html", "bno055__base_8py" ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "csvtouch.py", "csvtouch_8py.html", "csvtouch_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "lab1_vendotron.py", "lab1__vendotron_8py.html", "lab1__vendotron_8py" ],
    [ "lab2_reactiontime.py", "lab2__reactiontime_8py.html", "lab2__reactiontime_8py" ],
    [ "lab3UI.py", "lab3UI_8py.html", "lab3UI_8py" ],
    [ "lab4nucleo.py", "lab4nucleo_8py.html", "lab4nucleo_8py" ],
    [ "main_405_L4.py", "main__405__L4_8py.html", "main__405__L4_8py" ],
    [ "MCP9808.py", "MCP9808_8py.html", "MCP9808_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "touchdriver.py", "touchdriver_8py.html", "touchdriver_8py" ]
];