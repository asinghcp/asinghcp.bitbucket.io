var main_8py =
[
    [ "pushed", "main_8py.html#a7f9b5943aaea8de450990f1ae269e824", null ],
    [ "adc", "main_8py.html#afac357daa82cb1f4a2ca28df4f60bb14", null ],
    [ "ADC_range", "main_8py.html#aedf0fa435c2672b631339372e1a37693", null ],
    [ "buffer", "main_8py.html#a2888d28fa35f990cfa097aa3f9ecb184", null ],
    [ "Button", "main_8py.html#af1353e9ce6b67d29c0fe0cc0ae9a4317", null ],
    [ "extint", "main_8py.html#aed616b6b5cf07825f2f9f2e33ab071a5", null ],
    [ "flag", "main_8py.html#ae7d2c5272adf78050847f11905dddecd", null ],
    [ "freq", "main_8py.html#a3169a44fb7613809dbb7ae541c6dfe91", null ],
    [ "prebuffer", "main_8py.html#ab921ec908bdb62110baac96628fb7883", null ],
    [ "prebuffer_range", "main_8py.html#a21f48d01fa6191ddc2938a8456552e51", null ],
    [ "tim6", "main_8py.html#a34fa6cad72d11da527dcab62101a2eb8", null ],
    [ "time_data", "main_8py.html#a8d6a0486d90b42beb143dc94476b15fb", null ],
    [ "uart", "main_8py.html#a20f34167ce94fc3bd45d24f2af4d827a", null ],
    [ "user_input", "main_8py.html#ae7c698491682fba51e22768dcde986de", null ]
];